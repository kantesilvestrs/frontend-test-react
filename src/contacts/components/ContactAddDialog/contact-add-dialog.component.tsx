import React, { useState, useEffect } from "react";
import gql from "graphql-tag";
import IContact from "../../gql-client/model/IContact";
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button,
  withStyles,
  Theme,
  TextField,
  Box
} from "@material-ui/core";
import { Mutation, MutationFn } from "react-apollo";
import LoadingWrapper from "../../../components/LoadingWrapper/loading-wrapper.component";

const TextFieldWithStyles = withStyles((theme: Theme) => ({
  root: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200
  }
}))(TextField);

type addContactMutation = {
  addContact: IContact;
};

type addContactMutationVariables = {
  name: string;
  email: string;
};

const ADD_CONTACT = gql`
  mutation addContact($name: String, $email: String) {
    addContact(contact: { name: $name, email: $email }) {
      id
      name
      email
      __typename @include(if: false)
    }
  }
`;

export interface ContactAddDialogProps {
  open: boolean;
  onClose(params: any): void;
}

const ContactDeleteDialog: React.FunctionComponent<ContactAddDialogProps> = ({
  open,
  onClose,
  ...props
}) => {
  const [dialogOpen, setOpen] = useState<boolean>(open);

  useEffect(() => {
    if (dialogOpen != open) {
      setOpen(open);
    }
  }, [open]);

  const handleSubmit = async (
    event: React.FormEvent<HTMLFormElement>,
    addContact: MutationFn<addContactMutation, addContactMutationVariables>
  ) => {
    event.preventDefault();
    // const { namedItem } = event.currentTarget.elements;
    const { value: nameValue } = event.currentTarget.elements.namedItem(
      "name"
    ) as HTMLInputElement;
    const { value: emailValue } = event.currentTarget.elements.namedItem(
      "email"
    ) as HTMLInputElement;
    console.log(`${nameValue} - ${emailValue}`);
    await addContact({
      variables: {
        name: nameValue,
        email: emailValue
      }
    }).then(() => handleOnClose({ confirm: true }));
  };

  const handleOnClose = (params: any) => {
    setOpen(false);
    onClose(params);
  };

  return (
    <Dialog
      disableBackdropClick
      disableEscapeKeyDown
      maxWidth="xs"
      aria-labelledby="confirmation-dialog-title"
      open={open && dialogOpen}
      {...props}
    >
      <Mutation<{ addContact: IContact }> mutation={ADD_CONTACT}>
        {(addContact, { loading }) => {
          return (
            <React.Fragment>
              <DialogTitle id="confirmation-dialog-title">
                Add a new contact
              </DialogTitle>
              <LoadingWrapper loading={loading}>
                <form
                  noValidate
                  autoComplete="off"
                  onSubmit={e => handleSubmit(e, addContact)}
                >
                  <DialogContent dividers>
                    <Box
                      style={{
                        display: "flex",
                        flexWrap: "wrap"
                      }}
                    >
                      <TextFieldWithStyles
                        name="name"
                        id="contact-name"
                        label="Full Name"
                        data-testid="contactadddialog-component-name-field"
                      />
                      <TextFieldWithStyles
                        name="email"
                        id="contact-email"
                        label="Email"
                        data-testid="contactadddialog-component-email-field"
                      />
                    </Box>
                  </DialogContent>
                  <DialogActions>
                    <Button
                      color="primary"
                      onClick={() => handleOnClose({ confirm: false })}
                    >
                      Cancel
                    </Button>
                    <Button type="submit" color="secondary">
                      Create
                    </Button>
                  </DialogActions>
                </form>
              </LoadingWrapper>
            </React.Fragment>
          );
        }}
      </Mutation>
    </Dialog>
  );
};

export default ContactDeleteDialog;
